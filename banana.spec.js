jest.mock('./banana')

const bMock = require('./banana')
const bReal = jest.requireActual('./banana')

describe('bananas', () => {
  it('should be banana', () => {
    expect(bReal('help')).toBe('help banana');
  })

  it('should be banana mock', () => {
    expect(bMock('help')).toBe('help banana mock');
  })

  it('should be banana mockery', () => {
    bMock.mockImplementationOnce(word => `A real mockery of ${word}`)
    expect(bMock('banana')).toBe('A real mockery of banana')
  })

  it('should be banana mock again', () => {
    expect(bMock('help')).toBe('help banana mock')
  })
})
